package ro.tuc.ds2020.controllers;

import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MonitoringDTO;
import ro.tuc.ds2020.services.MonitoringService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/monitoring")
@CrossOrigin(origins = {"http://localhost", "http://localhost:3000"})
public class MonitoringController {

    private final MonitoringService monitoringService;

    public MonitoringController(MonitoringService monitoringService) {
        this.monitoringService = monitoringService;
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping("/{deviceId}")
    public ResponseEntity<List<MonitoringDTO>> getMonitoring(@PathVariable UUID deviceId) {
        List<MonitoringDTO> dtos = monitoringService.getMonitoring(deviceId);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
