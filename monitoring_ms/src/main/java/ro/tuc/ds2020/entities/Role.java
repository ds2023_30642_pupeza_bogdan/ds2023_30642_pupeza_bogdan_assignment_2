package ro.tuc.ds2020.entities;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}