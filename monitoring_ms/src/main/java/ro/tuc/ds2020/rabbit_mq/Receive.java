package ro.tuc.ds2020.rabbit_mq;


import com.rabbitmq.client.*;

import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Queue;

public class Receive {

    public final static String QUEUE_NAME = "monitoringQueue";
    public final static String TOPIC_EXCHANGE_NAME = "monitoringTopic";
    public final static boolean isTopic = true;

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        String queueName = QUEUE_NAME;
        if(isTopic) {
            channel.exchangeDeclare(TOPIC_EXCHANGE_NAME, BuiltinExchangeType.TOPIC, true);
            queueName = channel.queueDeclare().getQueue();
            channel.queueBind(queueName, TOPIC_EXCHANGE_NAME, "");
        } else {
            channel.queueDeclare(QUEUE_NAME,true, false, false, null);
        }
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            Map<String, String> map = MonitoringUtils.convertStringToMap(message);
            System.out.println(" [x] Received '" + map + "'");

        };

        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }
}