package ro.tuc.ds2020.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.rabbit_mq.MonitoringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Map;

@Configuration
public class RabbitMQConfig {

    public final static String MONITORING_QUEUE = "monitoringQueue";
    public final static String MONITORING_TOPIC = "monitoringTopic";
    public final static String DEVICES_QUEUE = "devicesQueue";
    public final static String DEVICES_TOPIC = "devicesTopic";

    private final CachingConnectionFactory cachingConnectionFactory;

    public RabbitMQConfig(CachingConnectionFactory cachingConnectionFactory) {
        this.cachingConnectionFactory = cachingConnectionFactory;
    }

    @Bean
    public Declarables  topicBinding() {
        Queue monitoringQueue = new Queue(MONITORING_QUEUE);
        Queue devicesQueue = new Queue(DEVICES_QUEUE);
        TopicExchange monitoringTopic = new TopicExchange(MONITORING_TOPIC);
        TopicExchange devicesTopic = new TopicExchange(DEVICES_TOPIC);
        return  new Declarables (
                monitoringQueue,
                monitoringTopic,
                BindingBuilder.bind(monitoringQueue).to(monitoringTopic).with(""),
                BindingBuilder.bind(devicesQueue).to(devicesTopic).with("")
        );
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(SimpleRabbitListenerContainerFactoryConfigurer configurer) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, cachingConnectionFactory);
        factory.setAcknowledgeMode(AcknowledgeMode.AUTO);
        factory.setDefaultRequeueRejected(false);
        return factory;
    }

    @Bean
    public MessageConverter converter() {
        return new MessageConverter() {
            @Override
            public Message toMessage(Object o, MessageProperties messageProperties) throws MessageConversionException {
                return new Message(o.toString().getBytes(), messageProperties);
            }

            @Override
            public Map<String, String> fromMessage(Message message) throws MessageConversionException {
                String messageString = new String(
                        message.getBody(),
                        StandardCharsets.UTF_8);
                MyLoggeer.print(messageString);
                return MonitoringUtils.convertStringToMap(messageString);
            }
        };
    }

    @Bean
    public RabbitTemplate rabbitTemplate(MessageConverter converter) {
        RabbitTemplate template = new RabbitTemplate(cachingConnectionFactory);
        template.setMessageConverter(converter);
        return template;
    }
}
