package ro.tuc.ds2020.web_socket;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.HtmlUtils;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import ro.tuc.ds2020.my_logger.MyLoggeer;

@Controller
@RequestMapping(value = "/api/v1/monitoring")

public class WebSocketController {
    static public final String TOPIC = "/topic/monitoring";
    private final SimpMessagingTemplate template;

    WebSocketController(SimpMessagingTemplate template){
        this.template = template;
    }

    @PostMapping("/send")
    public ResponseEntity<Void> sendMessage(@RequestBody WebSocketMessage message) {
        template.convertAndSend(TOPIC, message);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @MessageMapping("/hello")
    @SendTo(TOPIC)
    public WebSocketMessage greeting(WebSocketMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new WebSocketMessage("New message: "
                + HtmlUtils.htmlEscape(message.getContent()));
    }

    @MessageMapping("/user-message")
    @SendToUser("/queue/reply")
    public String sendBackToUser(@Payload String message /*, @Header("simpSessionId") String sessionId*/) {
        return "Only you have received this message: " + message;
    }

    @MessageMapping("/user-message-{userName}")
    public void sendToOtherUser(@RequestBody String message,
                                @PathVariable("userName") @DestinationVariable String userName) {
        template.convertAndSend("/queue/reply-" + userName, "You have a message from someone: " + message);
    }

    @PostMapping("/sendToUser/{userName}")
    public ResponseEntity<Void>  sendToUser(@RequestBody WebSocketMessage message, @PathVariable("userName") String userName) {
        MyLoggeer.print(userName);
        template.convertAndSend("/queue/user/" + userName, message);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}