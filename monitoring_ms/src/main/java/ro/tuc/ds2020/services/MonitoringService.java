package ro.tuc.ds2020.services;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.config.RabbitMQConfig;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.MonitoringDTO;
import ro.tuc.ds2020.entities.Monitoring;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.repositories.MeasurementValueRepository;
import ro.tuc.ds2020.web_socket.WebSocketMessage;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MonitoringService {

//    static public final String TOPIC = "/topic/monitoring";
    static public final String QUEUE = "/queue/monitoring/user";
    private final SimpMessagingTemplate template;


    private final MeasurementValueRepository measurementValueRepository;
    private final DeviceService deviceService;

    MonitoringService(SimpMessagingTemplate template, MeasurementValueRepository measurementValueRepository, DeviceService deviceService){
        this.template = template;
        this.measurementValueRepository = measurementValueRepository;
        this.deviceService = deviceService;
    }
    public List<MonitoringDTO> getMonitoring(UUID deviceId){
        List<Monitoring> monitoringList = measurementValueRepository.findBy(
                Example.of(Monitoring.builder().deviceId(deviceId).build()),
                FluentQuery.FetchableFluentQuery::all);
        return monitoringList.stream().map(
                        monitoring -> MonitoringDTO.builder().id(monitoring.getId())
                    .deviceId(monitoring.getDeviceId())
                    .timestamp(monitoring.getTimestamp())
                                .measurementValue(monitoring.getValue())
                    .build())
                .collect(Collectors.toList());
    }


    @RabbitListener(queues = {RabbitMQConfig.MONITORING_QUEUE})
    public void onMeasurementValueReceive(Map<String, String> message){
        Monitoring value = Monitoring.builder()
                .value(Double.parseDouble(message.get("measurement_value")))
                .deviceId(UUID.fromString(message.get("device_id")))
                .timestamp(message.get("timestamp"))
                .build();
        measurementValueRepository.save(value);

         DeviceDTO deviceDTO = deviceService.findDeviceById(value.getDeviceId());
        if(shouldNotifyUser(deviceDTO)){
            MyLoggeer.print("send to " + QUEUE  + ": " + String.valueOf(deviceDTO.getUserId()));
            template.convertAndSend(
                    QUEUE + "/" + deviceDTO.getUserId(),
                    new WebSocketMessage(message.toString())
            );
        }
    }

    private boolean shouldNotifyUser(DeviceDTO deviceDTO){
        List<MonitoringDTO> monitoringDTOList = getMonitoring(deviceDTO.getId());
        double value = monitoringDTOList.stream()
                .filter(e ->{
                    Timestamp t = new Timestamp(Long.parseLong(e.getTimestamp()));
                    return t.after(Timestamp.valueOf(LocalDateTime.now().minusMinutes(60)));
                })
                .mapToDouble(MonitoringDTO::getMeasurementValue).sum() / 60;
        return deviceDTO.getMhec() < (int) value;
    }
}
