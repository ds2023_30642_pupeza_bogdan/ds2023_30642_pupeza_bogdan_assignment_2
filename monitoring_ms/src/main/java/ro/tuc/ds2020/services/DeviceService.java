package ro.tuc.ds2020.services;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.config.RabbitMQConfig;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DeviceDTO;
import ro.tuc.ds2020.dtos.builders.DeviceBuilder;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.my_logger.MyLoggeer;
import ro.tuc.ds2020.repositories.DeviceRepository;

@Service
public class DeviceService {
    private final DeviceRepository deviceRepository;

    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public List<DeviceDTO> findDeviceByUserId(UUID userId) {
        List<Device> deviceList = deviceRepository.findByUserId(userId);
        return deviceList.stream()
                .map(DeviceBuilder::toDeviceDTO)
                .collect(Collectors.toList());
    }

    public DeviceDTO findDeviceById(UUID id) {
        Optional<Device> prosumerOptional = deviceRepository.findById(id);
        if (prosumerOptional.isEmpty()) {
            throw new ResourceNotFoundException(Device.class.getSimpleName() + " with id: " + id);
        }
        return DeviceBuilder.toDeviceDTO(prosumerOptional.get());
    }

    public void update(UUID deviceId, Device device) {
        int rowsUpdated = deviceRepository.updateDevice(deviceId,
                device.getUserId(),
                device.getDescription(),
                device.getAddress(),
                device.getMhec());
        MyLoggeer.print("Rows updated: " + rowsUpdated);
    }

    public void deleteById(UUID id) {
        deviceRepository.deleteById(id);
    }
    public void deleteByUserId(UUID userId) {
        List<DeviceDTO> deviceDTOList = findDeviceByUserId(userId);
        deviceRepository.deleteAllByIdInBatch(
                deviceDTOList.stream()
                        .map(DeviceDTO::getId)
                        .collect(Collectors.toList())
        );
    }

    @RabbitListener(queues = {RabbitMQConfig.DEVICES_QUEUE})
    public void onDeviceReceive(Map<String, String> map){
        Device device = DeviceBuilder.entityFromMap(map);
        if(Objects.equals(map.get("action"), "SAVE")) {
            Device saved = deviceRepository.save(device);
            MyLoggeer.print(saved.toString());
        } else if(Objects.equals(map.get("action"), "UPDATE")) {
            update(device.getId(), device);
        } else if (Objects.equals(map.get("action"), "DELETE")){
            deleteById(UUID.fromString(map.get("key")));
        }
    }
}
